Source: zigpy
Maintainer: Gard Spreemann <gspr@nonempty.org>
Section: python
Priority: optional
Standards-Version: 4.6.1.1
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-aiohttp,
               python3-aiosqlite (>= 0.16.0),
               python3-all,
               python3-crccheck,
               python3-cryptography,
               python3-serial-asyncio,
               python3-setuptools,
               python3-voluptuous,
               python3-yarl
Rules-Requires-Root: no
Homepage: https://github.com/zigpy/zigpy/
Vcs-Browser: https://salsa.debian.org/gspr/zigpy
Vcs-Git: https://salsa.debian.org/gspr/zigpy.git -b debian/sid

Package: python3-zigpy
Section: python
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Python Zigbee stack
 zigpy is a hardware independent Zigbee protocol stack integration
 project to implement Zigbee standard specifications as a Python 3
 library.
 .
 Zigbee integration via zigpy allows you to connect one of many
 off-the-shelf Zigbee Coordinator adapters using one of the available
 Zigbee radio library modules compatible with zigpy to control Zigbee
 based devices. There is currently support for controlling Zigbee
 device types such as binary sensors (e.g., motion and door sensors),
 sensors (e.g., temperature sensors), lights, switches, buttons,
 covers, fans, climate control equipment, locks, and intruder alarm
 system devices.
